var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    /*mix.copy('vendor/bower_components/slider.revolution.v5/css/', 'resources/assets/css/vendors/slider.revolution.v5/');
    mix.copy('vendor/bower_components/jquery/jquery-2.1.4.min.js', 'resources/assets/js/vendors/jquery/');
    mix.copy('vendor/bower_components/styleswitcher/styleswitcher.js', 'resources/assets/js/vendors/styleswitcher/styleswitcher.js');
    mix.copy('vendor/bower_components/slider.revolution.v5/js/', 'resources/assets/js/vendors/slider.revolution.v5/');*/
    mix.sass('app.scss', 'resources/assets/css/');
    mix.styles([
        'app.css',
        'vendors/slider.revolution.v5/pack.css'
    ]);
    mix.scripts([
        'vendors/jquery.js',
        'vendors/bootstrap.js'
    ]);
    mix.browserify("registration/app.js", "public/js/registration/app.js");
    mix.version(['css/all.css','js/registration/app.js']);
});
