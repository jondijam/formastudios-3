<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        //Visit the home page
        $this->visit('/');
        // Press a "Click me" link
        $this->click('Click Me');
        // See "You've been clicked, punk."
        $this->see("You've been clicked, punk.");
        // Assert that the current url is /feedback'/
        $this->seePageIs('/feedback');
    }
}