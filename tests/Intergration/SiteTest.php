<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SiteTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @test
    */
    public function an_user_create_a_new_site()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user)->visit('/')->type('demo.local','domain')->press('Save');
    }
}
