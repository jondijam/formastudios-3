<?php

namespace App\Http\Middleware;

use App\Domain;
use App\Site;
use Closure;
use Config;
class VerifyDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $domain = $request->getHost();
        $domain = Site::isRegistered($domain);

        if($domain)
        {
            return $next($request);
        }

        return redirect(Config::get('app.site'));
    }
}
