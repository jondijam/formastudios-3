<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Plan;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware("guest");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::with(['features'])->get();
        return view('home.index', compact('plans'));
    }
}
