<?php

namespace App\Http\Controllers\Sites;

use App\Page;
use App\Site;
use igaster\laravelTheme\Theme;
use igaster\laravelTheme\Themes;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $host = $request->getHost();
        $site = Site::with([])->currentHost($host)->first();
        $theme = $site->theme->name;
        $themes = new Themes();
        $themes->set($theme);

        $connection = explode('.', $host);
        $page = new Page();
        $page->setConnection($connection[0]);
        $pages = $page->all();


        return view('home.index');
        /*
        $page = new Page();
        $page->setConnection()

        return view('home.index');*/
    }
}
