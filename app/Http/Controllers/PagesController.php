<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * A home page for formastudios.com The first page that users see.
    */
    public function home()
    {
        return view('home');
    }
}
