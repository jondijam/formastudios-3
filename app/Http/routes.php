<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$domain = Request::getHost();

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () use($domain)
{
    if ($domain != Config::get('app.domain'))
    {
        Route::group(['domain'=> $domain, 'namespace'=>'Sites', 'as'=>"sites.", "middleware"=>["domain"]], function()
        {
            Route::group(['prefix'=> 'admin', 'namespace'=>'Admin'], function()
            {

            });

            Route::get("/", ["as"=>"home.index", "uses"=>"HomeController@index"]);
            Route::get('{pages}', [ 'as' => 'pages.show', 'uses'=>'PagesController@show'])->where('pages', '(.*)');
            Route::resource('pages', "PagesController");
            //Route::get('/', ['as'=>'home', 'uses' => 'PagesController@home']);
        });
    }
    elseif($domain == Config::get('app.domain'))
    {
        Route::group(['domain'=> Config::get('app.domain')], function()
        {
            Route::group(['middleware'=>'auth'], function()
            {
                Route::resource('sites', 'SitesController');
                Route::get('/dashboard', ['as'=>'dashboard', 'uses'=>"AdminController@index"]);
            });

            Route::get('/', ['as'=>'home', 'uses' => 'HomeController@index']);
            Route::get('about-us', ['as'=>'about-us', 'uses'=>'PagesController@aboutUs']);
            Route::auth();
            Route::resource('themes', 'ThemesController');


        });
    }
});
