<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    public function plans()
    {
        return $this->belongsToMany(Plan::class);
    }
}
