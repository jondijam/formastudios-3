<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 24.1.2016
 * Time: 10:41
 */

namespace App\Traits;
use Illuminate\Support\Facades\Crypt;


trait EncryptableTrait
{
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if(in_array($key, $this->encryptable))
        {
            $value = Crypt::decrypt($value);
        }
        return $value;
    }

    public function setAttribute($key, $value)
    {
        if(in_array($key, $this->encryptable))
        {
            $value = Crypt::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }
}