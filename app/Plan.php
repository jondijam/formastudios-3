<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Plan extends Model
{
    protected $fillable = ["braintree_id","braintree_plan","price"];

    public function features()
    {
        return $this->belongsToMany(Feature::class);
    }
}
