<?php

namespace App\Listeners;

use App\Events\SiteWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateDatabase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SiteWasCreated  $event
     * @return void
     */
    public function handle(SiteWasCreated $event)
    {

        $site = explode('.', $event->site->domain);
        $database = $site[0];

        \Config::set("database.connections", [$database=>[
            'driver'   => 'pgsql',
            'host'     => env('DB_HOST', 'localhost'),
            'database' => $database,
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public',
        ]]);

        \DB::statement('CREATE DATABASE :schema OWNER :owner', array('schema' => $database, 'owner'=>env('DB_USERNAME', 'forge')));
        \Artisan::call('migrate',['database'=>$database]);
    }
}
