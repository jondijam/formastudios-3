<?php

namespace App\Listeners;

use App\Events\SiteWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterDNS
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SiteWasCreated  $event
     * @return void
     */
    public function handle(SiteWasCreated $event)
    {
        //
    }
}
