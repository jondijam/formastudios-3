<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ["name", "quantity", "braintree_plan", "braintree_id"];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
