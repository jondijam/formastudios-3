<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function content()
    {
        return $this->hasMany(Content::class);
    }
}
