<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
