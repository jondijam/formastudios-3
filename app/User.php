<?php

namespace App;

use App\Traits\EncryptableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use EncryptableTrait, Billable;
    protected $encryptable = ['first_name', 'last_name'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    public function taxPercentage() {
        return 24;
    }

    public function fullName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sites()
    {
        return $this->hasMany(Site::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }


}
