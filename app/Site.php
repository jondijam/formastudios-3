<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = ["domain"];



    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeCurrentHost($query, $host)
    {
        return $query->where('domain', $host);
    }

    public static function isRegistered($domain)
    {
        $domain = static::with([])->where('domain', $domain)->first();

        if($domain)
        {
            return true;
        }
        return false;
    }
}
