<?php

namespace App\Console\Commands;

use Braintree\Plan;
use App\Plan as PlanApp;
use Illuminate\Console\Command;

class PlanConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'load plans from braintree';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $plans = Plan::all();

        foreach ($plans as $plan)
        {
            PlanApp::create(["braintree_id"=>$plan->id, 'braintree_plan'=>$plan->name, 'price'=>$plan->price]);
        }
    }
}
