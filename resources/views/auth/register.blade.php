@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <h1>Registration</h1>
                </div>
            </div>
        </div>
        <form class="form-horizontal"
              id="registration"
              role="form"
              data-client-token="{{ \Braintree\ClientToken::generate() }}"
              client-token="{{ \Braintree\ClientToken::generate() }}"
              method="POST"
              action="{{ url('/register') }}"
        >
            {!! csrf_field() !!}
            <div class="row" v-show="errorShow">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        @{{ errors.messages }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">Pick a plan</h2>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Plan</label>
                                <div class="col-md-6">
                                    <select name="plan" class="form-control">
                                        @foreach($plans as $plan)
                                            <option @if(str_slug($plan->braintree_plan) == $currentPlan) selected="selected" @endif value="{{ $plan->id }}">{{ $plan->braintree_plan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="card-number" class="col-md-4 control-label">Card Number</label>
                                <div class="col-md-6">
                                    <div id="card-number" class="form-control"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cvv" class="col-md-4 control-label">CVV</label>
                                <div class="col-md-6">
                                    <div id="cvv" class="form-control"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="expiration-date" class="col-md-4 control-label">Expiration Date</label>
                                <div class="col-md-6">
                                    <div id="expiration-date" class="form-control"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h2 class="panel-title">Register</h2></div>
                        <div class="panel-body">
                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">First name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Last name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts.footer')
    <script src="{{ elixir('js/registration/app.js') }}"></script>
@stop