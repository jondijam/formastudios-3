<!Document html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Formastudios - create your website is easy.</title>
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet" />
</head>
<body>
<header>
    <nav class="navbar navbar-formastudios navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button"
                        class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target="#main-menu"
                        aria-expanded="false"
                >
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">FORMASTUDIOS</a>
            </div>
            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav navbar-menu">
                    @if (Auth::check())
                        <li><a href="{{ route('sites.index') }}">Sites</a></li>
                    @endif
                    <li><a href="{{ route('themes.index') }}">Themes</a></li>
                    <li><a href="{{ route('about-us') }}">About us</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                    <li><a class="btn btn-primary" href="/login">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->fullName() }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>
<div class="main" id="wrapper">
    @yield('slide.show')
    @yield('headline')
    @yield('content')
</div>
<footer>
</footer>
<script src="{{ asset('js/all.js') }}"></script>
@yield('scripts.footer')
</body>
</html>