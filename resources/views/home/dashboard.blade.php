@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
        @if($sites)
        <div class="row">
            <div class="col-sm-12">
               <div class="panel panel-default">
                   <div class="panel-heading">
                       <h2 class="panel-title">My Sites</h2>
                   </div>
                   <div class="panel-body">
                       <table class="table">
                           <thead>
                           <tr>
                               <th>Domain</th>
                               <th>Action</th>
                           </tr>
                           </thead>
                           <tbody>
                           @foreach($sites as $site)
                           <tr>
                               <td>{{ $site->domain }}</td>
                               <td><a href="http://{{ $site->domain }}" target="_blank">View site</a></td>
                           </tr>
                           @endforeach
                           </tbody>
                       </table>
                   </div>
               </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-sm-12">
                <form action="{{ route('sites.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Select a theme</h3>
                        </div>
                        <div class="panel-body">
                            @foreach($themes as $theme)
                                <div class="col-sm-6 col-md-4">
                                    <div class="thumbnail">
                                        <img src="{{ $theme->thumbnail }}" alt="{{ $theme->name }}">
                                        <div class="caption">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="theme_id" id="theme-{{ $theme->id }}" value="{{ $theme->id }}" checked>
                                                    {{ $theme->name }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">Create a site</h2>
                        </div>
                        <div class="panel-body">

                            <div></div>
                            <div class="form-group form-group-lg">
                                <label>Domain</label>
                                <input type="text" name="domain" class="form-control" value="" />
                            </div>

                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endif
    </div>
    {{--
     Newest theme showed
     Create sites
     News from formastudios
    --}}
@stop