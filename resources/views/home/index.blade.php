@extends('layouts.master')
@section("slide.show")
    <div class="slider-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slider-group">
                        <div class="slider">
                            <h1 class="slider-featured-text">
                                We build, and we <span style="font-weight: normal">know how <br /> to create</span>  <span style="color:#1abc9c">great</span> websites...
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('content')
    <section id="subscription">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-center">Select a plan</h2>
                </div>
            </div>
            <div class="row pricetable-container">
                @foreach($plans as $plan)
                    <div class="col-md-4 price-table">
                        <h3>{{ $plan->braintree_plan }}</h3>
                        <p>{{ money_format('%i', $plan->price) }} <span>Per month</span></p>
                        <ul>
                            @foreach($plan->features as $feature)
                            <li>{{ $feature->title }}</li>
                            @endforeach
                        </ul>
                        <a href="/register/?plan={{ str_slug($plan->braintree_plan) }}" class="btn btn-primary">Subscribe</a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@stop