import braintree from "braintree-web";
import Vue from "vue";
import VueResource from "vue-resource";
Vue.use(VueResource);

let vueRegistration = new Vue({
    el:"#registration",
    props:["clientToken"],
    data:{
        errorShow:false,
        errors:{
            messages:""
        }
    },
    ready:function () {
        let vm = this;

    }
});

braintree.setup(vueRegistration.$get('clientToken'), "custom", {
    id: "registration",
    hostedFields: {
        number: {
            selector: "#card-number"
        },
        cvv: {
            selector: "#cvv"
        },
        expirationDate: {
            selector: "#expiration-date",
            placeholder:"04/2017"
        }
    },
    onError:function (response)
    {
        console.log(response);
        vueRegistration.$set('errors.messages', response.message);
        vueRegistration.$set('errorShow', true);
        /*
        vm.errorShow = true;
        vm.errors.messages = message;*/
    }
});

